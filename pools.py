import json
import os
from flask import request, abort, Response
from flask import Flask, render_template
import datetime 
import requests
import xml.etree.ElementTree as ET


application = Flask(__name__)
app = application
all_pools = []
print(datetime.datetime.now())

#Initializations
url = "https://raw.githubusercontent.com/devdattakulkarni/elements-of-web-programming/master/data/austin-pool-timings.xml"
data = requests.get(url)
root = ET.fromstring(data.text)
pool_dict_list = []

# Parsing XML Data and Create Dictionary of Pools, and their info
for pool in root.findall("row"):
    temp_dict = {}

    #Go through list and try accessing values 
    try:
        for temp in ["pool_name", "status", "phone", "pool_type", "open_date", 'weekday', 'weekend']:
            try:
                temp_dict[temp] = pool.find(temp).text
            except:
                temp_dict[temp] = None

    except AttributeError:
        continue 
    pool_dict_list.append(temp_dict)
### END NEW CODE ###


@app.route("/pools")
def get_pools():

    all_pools = []
    pools = pool_dict_list

    for i in pools:
        pool = {}
        pool['Name'] = i['pool_name']
        pool['PoolType'] = i['pool_type']
        pool['Status'] = i['status']
        pool['Phone'] = i['phone']
        pool['OpenDate'] = i['open_date']
        pool["Weekday"] = i['weekday']
        pool["Weekend"] = i['weekend']
        all_pools.append(pool)
    return(json.dumps(all_pools))


@app.route("/")
def pool_info_website():
    return render_template('index.html')


if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0')
